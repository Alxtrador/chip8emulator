use nom::{
    bits::{bits, complete::{take, tag}},
    branch::alt,
    combinator::{map},
    error::Error,
    IResult,
    sequence::{pair, preceded, terminated, tuple},
};

#[derive(Debug, PartialEq)]
pub enum Opcode {
    Cls, //00E0
    Ret, //00EE
    Jmp(u16), //1nnn
    Call(u16), //2nnn
    SeB(u8, u8), //3xkk
    Sne(u8, u8), //4xkk
    SeV(u8, u8), //5xy0
    LdB(u8, u8), //6xkk
    AddB(u8, u8), //7xkk
    LdV(u8, u8), //8xy0
    OrV(u8, u8), //8xy1
    AndV(u8, u8), //8xy2
    XorV(u8, u8), //8xy3
    AddV(u8, u8), //8xy4
    SubV(u8, u8), //8xy5
    ShrV(u8, u8), //8xy6
    SubN(u8, u8), //8xy7
    ShlV(u8, u8), //8xyE
    SneV(u8, u8), //9xy0
    LdI(u16), //Annn
    JmpV(u16), //Bnnn
    Rnd(u8, u8), //Cxkk
    Drw(u8, u8, u8), //Dxyn
    Skp(u8), //Ex9E
    Sknp(u8), //ExA1
    LdDT(u8), //Fx07
    LdK(u8), //Fx0A
    LdDTV(u8), //Fx15
    LdST(u8), //Fx18
    AddI(u8), //Fx1E
    LdF(u8), //Fx29
    LdBV(u8), //Fx33
    LdIV(u8), //Fx55
    LdVI(u8), //Fx65
}

pub fn parse(input: &[u8]) -> Opcode {
    let (_, res) = bits::<_, _, _, Error<_>, _>(parse_bits)(input).unwrap();
    res
}

fn parse_bits(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    parse_opcode(input)
}

fn parse_opcode(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    alt((
        parse_cls,
        parse_ret,
        parse_jmp,
        parse_call,
        parse_seb,
        parse_sne,
        parse_sev,
        parse_ldb,
        parse_addb,
        parse_ldv,
        parse_orv,
        parse_andv,
        parse_xorv,
        parse_addv,
        parse_subv,
        parse_shrv,
        parse_subn,
        parse_shlv,
        parse_snev,
        alt((
            parse_ldi, 
            parse_jmpv, 
            parse_rnd, 
            parse_drw, 
            parse_skp,
            parse_sknp,
            parse_lddt, 
            parse_ldk, 
            parse_lddtv, 
            parse_ldst, 
            parse_addi, 
            parse_ldf, 
            parse_ldbv, 
            parse_ldiv, 
            parse_ldvi,
        )),
    ))(input)
}

fn parse_cls(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(tag(0x00E0, 16usize), |_| Opcode::Cls)(input)
}

fn parse_ret(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(tag(0x00EE, 16usize), |_| Opcode::Ret)(input)
}

fn parse_jmp(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0x1, 4usize), take(12usize)), |jmp| Opcode::Jmp(jmp)
    )(input)
}

fn parse_call(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0x2, 4usize), take(12usize)), |call| Opcode::Call(call)
    )(input)
}

fn parse_seb(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0x3, 4usize), pair(take(4usize), take(8usize))), |(x, kk)| Opcode::SeB(x, kk)
    )(input)
}

fn parse_sne(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0x4, 4usize), pair(take(4usize), take(8usize))), |(x, kk)| Opcode::Sne(x, kk)
    )(input)
}

fn parse_sev(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0x5, 4usize), pair(take(4usize), terminated(take(4usize), tag(0x0, 4usize)))), |(x, y)| Opcode::SeV(x, y)
    )(input)
}

fn parse_ldb(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0x6, 4usize), pair(take(4usize), take(8usize))), |(x, kk)| Opcode::LdB(x, kk)
    )(input)
}

fn parse_addb(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0x7, 4usize), pair(take(4usize), take(8usize))), |(x, kk)| Opcode::AddB(x, kk)
    )(input)
}

fn parse_ldv(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0x8, 4usize), pair(take(4usize), terminated(take(4usize), tag(0x0, 4usize)))), |(x, y)| Opcode::LdV(x, y)
    )(input)
}

fn parse_orv(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0x8, 4usize), pair(take(4usize), terminated(take(4usize), tag(0x1, 4usize)))), |(x, y)| Opcode::OrV(x, y)
    )(input)
}

fn parse_andv(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0x8, 4usize), pair(take(4usize), terminated(take(4usize), tag(0x2, 4usize)))), |(x, y)| Opcode::AndV(x, y)
    )(input)
}

fn parse_xorv(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0x8, 4usize), pair(take(4usize), terminated(take(4usize), tag(0x3, 4usize)))), |(x, y)| Opcode::XorV(x, y)
    )(input)
}

fn parse_addv(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0x8, 4usize), pair(take(4usize), terminated(take(4usize), tag(0x4, 4usize)))), |(x, y)| Opcode::AddV(x, y)
    )(input)
}

fn parse_subv(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0x8, 4usize), pair(take(4usize), terminated(take(4usize), tag(0x5, 4usize)))), |(x, y)| Opcode::SubV(x, y)
    )(input)
}

fn parse_shrv(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0x8, 4usize), pair(take(4usize), terminated(take(4usize), tag(0x6, 4usize)))), |(x, y)| Opcode::ShrV(x, y)
    )(input)
}

fn parse_subn(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0x8, 4usize), pair(take(4usize), terminated(take(4usize), tag(0x7, 4usize)))), |(x, y)| Opcode::SubN(x, y)
    )(input)
}

fn parse_shlv(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0x8, 4usize), pair(take(4usize), terminated(take(4usize), tag(0xE, 4usize)))), |(x, y)| Opcode::ShlV(x, y)
    )(input)
}

fn parse_snev(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0x9, 4usize), pair(take(4usize), terminated(take(4usize), tag(0x0, 4usize)))), |(x, y)| Opcode::SneV(x, y)
    )(input)
}

fn parse_ldi(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0xA, 4usize), take(12usize)), |i| Opcode::LdI(i)
    )(input)
}

fn parse_jmpv(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0xB, 4usize), take(12usize)), |jmp| Opcode::JmpV(jmp)
    )(input)
}

fn parse_rnd(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0xC, 4usize), pair(take(4usize), take(8usize))), |(x, kk)| Opcode::Rnd(x, kk)
    )(input)
}

fn parse_drw(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0xD, 4usize), tuple((take(4usize), take(4usize), take(4usize)))), |(x, y, n)| Opcode::Drw(x, y, n)
    )(input)
}

fn parse_skp(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0xE, 4usize), terminated(take(4usize), tag(0x9E, 8usize))), |x| Opcode::Skp(x)
    )(input)
}

fn parse_sknp(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0xE, 4usize), terminated(take(4usize), tag(0xA1, 8usize))), |x| Opcode::Sknp(x)
    )(input)
}

fn parse_lddt(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0xF, 4usize), terminated(take(4usize), tag(0x07, 8usize))), |x| Opcode::LdDT(x)
    )(input)
}

fn parse_ldk(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0xF, 4usize), terminated(take(4usize), tag(0x0A, 8usize))), |x| Opcode::LdK(x)
    )(input)
}

fn parse_lddtv(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0xF, 4usize), terminated(take(4usize), tag(0x15, 8usize))), |x| Opcode::LdDTV(x)
    )(input)
}

fn parse_ldst(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0xF, 4usize), terminated(take(4usize), tag(0x18, 8usize))), |x| Opcode::LdST(x)
    )(input)
}

fn parse_addi(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0xF, 4usize), terminated(take(4usize), tag(0x1E, 8usize))), |x| Opcode::AddI(x)
    )(input)
}

fn parse_ldf(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0xF, 4usize), terminated(take(4usize), tag(0x29, 8usize))), |x| Opcode::LdF(x)
    )(input)
}

fn parse_ldbv(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0xF, 4usize), terminated(take(4usize), tag(0x33, 8usize))), |x| Opcode::LdBV(x)
    )(input)
}

fn parse_ldiv(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0xF, 4usize), terminated(take(4usize), tag(0x55, 8usize))), |x| Opcode::LdIV(x)
    )(input)
}

fn parse_ldvi(input: (&[u8], usize)) -> IResult<(&[u8], usize), Opcode> {
    map(
        preceded(tag(0xF, 4usize), terminated(take(4usize), tag(0x65, 8usize))), |x| Opcode::LdVI(x)
    )(input)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parse_cls() {
        let t = [0x00, 0xE0];
        let (_, res) = bits::<_, _, _, Error<_>, _>(parse_cls)(&t).unwrap();
        assert_eq!(res, Opcode::Cls);
    }

    #[test]
    fn test_parse_ret() {
        let t = [0x00, 0xEE];
        let (_, res) = bits::<_, _, _, Error<_>, _>(parse_ret)(&t).unwrap();
        assert_eq!(res, Opcode::Ret);
    }

    #[test]
    fn test_parse_jmp() {
        let t = [0x15, 0xA4];
        let (_, res) = bits::<_, _, _, Error<_>, _>(parse_jmp)(&t).unwrap();
        assert_eq!(res, Opcode::Jmp(0x05A4));
    }

    #[test]
    fn test_parse_call() {
        let t = [0x2F, 0x9E];
        let (_, res) = bits::<_, _, _, Error<_>, _>(parse_call)(&t).unwrap();
        assert_eq!(res, Opcode::Call(0x0F9E));
    }

    #[test]
    fn test_parse_seb() {
        let t = [0x32, 0x22];
        let (_, res) = bits::<_, _, _, Error<_>, _>(parse_seb)(&t).unwrap();
        assert_eq!(res, Opcode::SeB(0x02, 0x22));
    }

    #[test]
    fn test_parse_sne() {
        let t = [0x42, 0x22];
        let (_, res) = bits::<_, _, _, Error<_>, _>(parse_sne)(&t).unwrap();
        assert_eq!(res, Opcode::Sne(0x02, 0x22));
    }

    #[test]
    fn test_parse_sev() {
        let t = [0x52, 0x20];
        let (_, res) = bits::<_, _, _, Error<_>, _>(parse_sev)(&t).unwrap();
        assert_eq!(res, Opcode::SeV(0x02, 0x02));
    }

    #[test]
    fn test_parse_ldb() {
        let t = [0x62, 0x22];
        let (_, res) = bits::<_, _, _, Error<_>, _>(parse_ldb)(&t).unwrap();
        assert_eq!(res, Opcode::LdB(0x02, 0x22));
    }

    #[test]
    fn test_parse_addb() {
        let t = [0x72, 0x22];
        let (_, res) = bits::<_, _, _, Error<_>, _>(parse_addb)(&t).unwrap();
        assert_eq!(res, Opcode::AddB(0x02, 0x22));
    }

    #[test]
    fn test_parse_ldv() {
        let t = [0x82, 0x20];
        let (_, res) = bits::<_, _, _, Error<_>, _>(parse_ldv)(&t).unwrap();
        assert_eq!(res, Opcode::LdV(0x02, 0x02));
    }

    #[test]
    fn test_parse_orv() {
        let t = [0x82, 0x21];
        let (_, res) = bits::<_, _, _, Error<_>, _>(parse_orv)(&t).unwrap();
        assert_eq!(res, Opcode::OrV(0x02, 0x02));
    }

    #[test]
    fn test_parse_andv() {
        let t = [0x82, 0x22];
        let (_, res) = bits::<_, _, _, Error<_>, _>(parse_andv)(&t).unwrap();
        assert_eq!(res, Opcode::AndV(0x02, 0x02));
    }

    #[test]
    fn test_parse_xorv() {
        let t = [0x82, 0x23];
        let (_, res) = bits::<_, _, _, Error<_>, _>(parse_xorv)(&t).unwrap();
        assert_eq!(res, Opcode::XorV(0x02, 0x02));
    }

    #[test]
    fn test_parse_addv() {
        let t = [0x82, 0x24];
        let (_, res) = bits::<_, _, _, Error<_>, _>(parse_addv)(&t).unwrap();
        assert_eq!(res, Opcode::AddV(0x02, 0x02));
    }

    #[test]
    fn test_parse_skp() {
        let t = [0xE0, 0x9E];
        let (_, res) = bits::<_, _, _, Error<_>, _>(parse_skp)(&t).unwrap();
        assert_eq!(res, Opcode::Skp(0x00));
    }
}