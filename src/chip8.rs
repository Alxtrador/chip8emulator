use super::parser::{parse, Opcode};

use rand::Rng;
use std::sync::{Arc, Mutex};

#[derive(Debug, PartialEq)]
pub struct Chip8 { 
    mem: [u8; 4096],
    gen_regs: [u8; 16],
    i_reg: u16,
    pub delay_reg: u8,
    sound_reg: u8,
    pc: u16,
    sp: u8,
    stack: [u16; 16],
}

impl Default for Chip8 {
    fn default() -> Self { 
        let mut mem = [0; 4096];
        //Sprite for "0"
        mem[0] = 0xF0;
        mem[1] = 0x90;
        mem[2] = 0x90;
        mem[3] = 0x90;
        mem[4] = 0xF0;
        //Sprite for "1"
        mem[5] = 0x20;
        mem[6] = 0x60;
        mem[7] = 0x20;
        mem[8] = 0x20;
        mem[9] = 0x70;
        //Sprite for "2"
        mem[10] = 0xF0;
        mem[11] = 0x10;
        mem[12] = 0xF0;
        mem[13] = 0x80;
        mem[14] = 0xF0;
        //Sprite for "3"
        mem[15] = 0xF0;
        mem[16] = 0x10;
        mem[17] = 0xF0;
        mem[18] = 0x10;
        mem[19] = 0xF0;
        //Sprite for "4"
        mem[20] = 0x90;
        mem[21] = 0x90;
        mem[22] = 0xF0;
        mem[23] = 0x10;
        mem[24] = 0x10;
        //Sprite for "5"
        mem[25] = 0xF0;
        mem[26] = 0x80;
        mem[27] = 0xF0;
        mem[28] = 0x10;
        mem[29] = 0xF0;
        //Sprite for "6"
        mem[30] = 0xF0;
        mem[31] = 0x80;
        mem[32] = 0xF0;
        mem[33] = 0x90;
        mem[34] = 0xF0;
        //Sprite for "7"
        mem[35] = 0xF0;
        mem[36] = 0x10;
        mem[37] = 0x20;
        mem[38] = 0x20;
        mem[39] = 0x40;
        //Sprite for "8"
        mem[40] = 0xF0;
        mem[41] = 0x90;
        mem[42] = 0xF0;
        mem[43] = 0x90;
        mem[44] = 0xF0;
        //Sprite for "9"
        mem[45] = 0xF0;
        mem[46] = 0x90;
        mem[47] = 0xF0;
        mem[48] = 0x10;
        mem[49] = 0xF0;
        //Sprite for "A"
        mem[50] = 0xF0;
        mem[51] = 0x90;
        mem[52] = 0xF0;
        mem[53] = 0x90;
        mem[54] = 0x90;
        //Sprite for "B"
        mem[55] = 0xE0;
        mem[56] = 0x90;
        mem[57] = 0xE0;
        mem[58] = 0x90;
        mem[59] = 0xE0;
        //Sprite for "C"
        mem[60] = 0xF0;
        mem[61] = 0x80;
        mem[62] = 0x80;
        mem[63] = 0x80;
        mem[64] = 0xF0;
        //Sprite for "D"
        mem[65] = 0xE0;
        mem[66] = 0x90;
        mem[67] = 0x90;
        mem[68] = 0x90;
        mem[69] = 0xE0;
        //Sprite for "E"
        mem[70] = 0xF0;
        mem[71] = 0x80;
        mem[72] = 0xF0;
        mem[73] = 0x80;
        mem[74] = 0xF0;
        //Sprite for "F"
        mem[75] = 0xF0;
        mem[76] = 0x80;
        mem[77] = 0xF0;
        mem[78] = 0x80;
        mem[79] = 0x80;

        Chip8 {
            mem,
            gen_regs: [0; 16],
            i_reg: 0,
            delay_reg: 0,
            sound_reg: 0,
            pc: 0x200,
            sp: 0,
            stack: [0; 16],
        }
    }
}

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum KeyCommand {
    Zero,
    One,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    A,
    B,
    C,
    D,
    E,
    F,
}



impl KeyCommand {
    fn get_val(self) -> u8 {
        match self {
            KeyCommand::Zero => 0x0,
            KeyCommand::One => 0x1,
            KeyCommand::Two => 0x2,
            KeyCommand::Three => 0x3,
            KeyCommand::Four => 0x4,
            KeyCommand::Five => 0x5,
            KeyCommand::Six => 0x6,
            KeyCommand::Seven => 0x7,
            KeyCommand::Eight => 0x8,
            KeyCommand::Nine => 0x9,
            KeyCommand::A => 0xA,
            KeyCommand::B => 0xB,
            KeyCommand::C => 0xC,
            KeyCommand::D => 0xD,
            KeyCommand::E => 0xE,
            KeyCommand::F => 0xF,
        }
    }
}

impl Chip8 {
    pub fn load_rom(&mut self, rom: Vec<u8>) {
        let mut i = 0x200;
        for byte in rom {
            self.mem[i] = byte;
            i += 1;
        }
    }

    fn sprite_mem_location(&self, x: u8) -> u16 {
        match self.gen_regs[x as usize] {
            0x00 => 0x0000,
            0x01 => 0x0005,
            0x02 => 0x000A,
            0x03 => 0x000F,
            0x04 => 0x0014,
            0x05 => 0x0019,
            0x06 => 0x001E,
            0x07 => 0x0023,
            0x08 => 0x0028,
            0x09 => 0x002D,
            0x0A => 0x0032,
            0x0B => 0x0037,
            0x0C => 0x003C,
            0x0D => 0x0041,
            0x0E => 0x0046,
            0x0F => 0x004B,
            _ => panic!()
        }
    }

    pub fn get_instr(&self) -> Opcode {
        let instr_bytes: [u8; 2] = [self.mem[self.pc as usize], self.mem[self.pc as usize + 1]];
        parse(&instr_bytes)
    }

    pub fn run_instr(&mut self, instr: Opcode, frame_buffer_arc: &Arc<Mutex<[bool; 64*32]>>,
            key_receiver: &Arc<Mutex<std::option::Option<KeyCommand>>>) {
        match instr {
            Opcode::Cls => {
                let mut frame_buffer = frame_buffer_arc.lock().unwrap();
                for i in 0..frame_buffer.len() {
                    frame_buffer[i] = false;
                }
                self.pc += 2;
            },
            Opcode::Ret => {
                self.sp -= 1;
                self.pc = self.stack[self.sp as usize];
            },
            Opcode::Jmp(jmp) => {
                self.pc = jmp;
            },
            Opcode::Call(call) => {
                self.stack[self.sp as usize] = self.pc + 2;
                self.sp += 1;
                self.pc = call;
            },
            Opcode::SeB(x, kk) => {
                if self.gen_regs[x as usize] == kk {
                    self.pc += 4;
                } else {
                    self.pc += 2;
                }
            },
            Opcode::Sne(x, kk) => {
                if self.gen_regs[x as usize] != kk {
                    self.pc += 4;
                } else {
                    self.pc += 2;
                }
            },
            Opcode::SeV(x, y) => {
                if self.gen_regs[x as usize] == self.gen_regs[y as usize] {
                    self.pc += 4;
                } else {
                    self.pc += 2;
                }
            },
            Opcode::LdB(x, kk) => {
                self.gen_regs[x as usize] = kk;
                self.pc += 2;
            },
            Opcode::AddB(x, kk) => {
                self.gen_regs[x as usize] = self.gen_regs[x as usize].wrapping_add(kk);
                self.pc += 2;
            },
            Opcode::LdV(x, y) => {
                self.gen_regs[x as usize] = self.gen_regs[y as usize];
                self.pc += 2;
            },
            Opcode::OrV(x, y) => {
                self.gen_regs[x as usize] |= self.gen_regs[y as usize];
                self.pc += 2;
            },
            Opcode::AndV(x, y) => {
                self.gen_regs[x as usize] &= self.gen_regs[y as usize];
                self.pc += 2;
            },
            Opcode::XorV(x, y) => {
                self.gen_regs[x as usize] ^= self.gen_regs[y as usize];
                self.pc += 2;
            },
            Opcode::AddV(x, y) => {
                self.gen_regs[x as usize] = self.gen_regs[x as usize].wrapping_add(self.gen_regs[y as usize]);
                let overflow_check = self.gen_regs[x as usize] as u16 + self.gen_regs[y as usize] as u16;
                self.gen_regs[0xF] = if overflow_check > u8::MAX as u16 { 1 } else { 0 };
                self.pc += 2;
            },
            Opcode::SubV(x, y) => {
                self.gen_regs[0xF] = if self.gen_regs[x as usize] > self.gen_regs[y as usize] { 1 } else { 0 };
                self.gen_regs[x as usize] = self.gen_regs[x as usize].wrapping_sub(self.gen_regs[y as usize]);
                self.pc += 2;
            },
            Opcode::ShrV(x, y) => {
                self.gen_regs[0xF] = if self.gen_regs[x as usize] & 0x01 == 0x01 { 1 } else { 0 };
                self.gen_regs[x as usize] = self.gen_regs[y as usize] >> 1;
                self.pc += 2;
            },
            Opcode::SubN(x, y) => {
                self.gen_regs[0xF] = if self.gen_regs[y as usize] > self.gen_regs[x as usize] { 1 } else { 0 };
                self.gen_regs[x as usize] = self.gen_regs[y as usize].wrapping_sub(self.gen_regs[x as usize]);
                self.pc += 2;
            },
            Opcode::ShlV(x, y) => {
                self.gen_regs[0xF] = if self.gen_regs[x as usize] & 0x80 == 0x80 { 1 } else { 0 };
                self.gen_regs[x as usize] = self.gen_regs[y as usize] << 1;
                self.pc += 2;
            },
            Opcode::SneV(x, y) => {
                if self.gen_regs[x as usize] != self.gen_regs[y as usize] {
                    self.pc += 4;
                } else {
                    self.pc += 2;
                }
            },
            Opcode::LdI(x) => {
                self.i_reg = x;
                self.pc += 2;
            },
            Opcode::JmpV(x) => {
                self.pc = self.gen_regs[0] as u16 + x;
            },
            Opcode::Rnd(x, y) => {
                let mut rng = rand::thread_rng();
                let rand: u8 = rng.gen();
                self.gen_regs[x as usize] = rand & y;
                self.pc += 2;
            },
            Opcode::Drw(vx, vy, n) => {
                let mut frame_buffer = frame_buffer_arc.lock().unwrap();
                let x = self.gen_regs[vx as usize] as usize % 64;
                let mut y = self.gen_regs[vy as usize] as usize % 32;
                self.gen_regs[0xF] = 0;
                for byte in &self.mem[self.i_reg as usize..(self.i_reg as usize + n as usize)] {
                    for i in 0..8 {
                        let bit = (*byte >> (7-i)) & 0x1 == 0x1;
                        if frame_buffer[((x+i) + y * 64) % 2047] == true && bit == true {
                            self.gen_regs[0xF] = 0x01;
                        }
                        frame_buffer[((x+i) + y * 64) % 2047] ^= bit;
                    }
                    y += 1;
                    if y > 32 {
                        break;
                    }
                }
                self.pc += 2;
            },
            Opcode::Skp(x) => {
                let key = key_receiver.lock().unwrap();
                match *key {
                    Some(key) => {
                        if key.get_val() == x {
                            self.pc += 4;
                        } else {
                            self.pc += 2;
                        }
                    },
                    None => {
                        self.pc += 2;
                    },
                }
            },
            Opcode::Sknp(x) => {
                let key = key_receiver.lock().unwrap();
                match *key {
                    Some(key) => {
                        if key.get_val() != x {
                            self.pc += 4;
                        } else {
                            self.pc += 2;
                        }
                    },
                    None => {
                        self.pc += 4;
                    },
                }
            },
            Opcode::LdDT(x) => {
                self.gen_regs[x as usize] = self.delay_reg;
                self.pc += 2;
            },
            Opcode::LdK(x) => {
                let key = key_receiver.lock().unwrap();
                match *key {
                    Some(val) => {
                        self.gen_regs[x as usize] = val.get_val();
                        self.pc += 2;
                    },
                    None => (),
                }
            },
            Opcode::LdDTV(x) => {
                self.delay_reg = self.gen_regs[x as usize];
                self.pc += 2;
            },
            Opcode::LdST(x) => {
                self.sound_reg = self.gen_regs[x as usize];
                self.pc += 2;
            },
            Opcode::AddI(x) => {
                self.i_reg += self.gen_regs[x as usize] as u16;
                self.pc += 2;
            },
            Opcode::LdF(x) => {
                self.i_reg = self.sprite_mem_location(x);
                self.pc += 2;
            },
            Opcode::LdBV(x) => {
                let hundreds = self.gen_regs[x as usize] / 100;
                let tens = (self.gen_regs[x as usize] / 10) % 10;
                let ones = self.gen_regs[x as usize] % 10;
                self.mem[self.i_reg as usize] = hundreds;
                self.mem[self.i_reg as usize + 1] = tens;
                self.mem[self.i_reg as usize + 2] = ones;
                self.pc += 2;
            },
            Opcode::LdIV(x) => {
                for i in 0..(x+1) as usize {
                    self.mem[self.i_reg as usize + i] = self.gen_regs[i as usize];
                }
                self.pc += 2;
            },
            Opcode::LdVI(x) => {
                for i in 0..(x+1) as usize {
                    self.gen_regs[i as usize] = self.mem[self.i_reg as usize + i];
                }
                self.pc += 2;
            },
        }
    }

    pub fn apply_60hz(&mut self) {
        if self.delay_reg > 0 {
            self.delay_reg -= 1;
        }
        if self.sound_reg > 0 {
            self.play_sound();
            self.sound_reg -= 1;
        } 
    }

    fn play_sound(&self) {
        todo!()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_run_instr_call_ret() {
        let mut chip8: Chip8 = Default::default();
        let frame_buffer_arc = Arc::new(Mutex::new([false; 64*32]));
        let latest_key = Arc::new(Mutex::new(None));
        chip8.run_instr(Opcode::Call(0x28E), &frame_buffer_arc, &latest_key);
        assert_eq!(chip8.sp, 1);
        assert_eq!(chip8.stack[0], 0x200);
        assert_eq!(chip8.pc, 0x28E);
        chip8.run_instr(Opcode::Ret, &frame_buffer_arc, &latest_key);
        assert_eq!(chip8.sp, 0);
        assert_eq!(chip8.pc, 0x200);
    }

    #[test]
    fn test_run_instr_jmp() {
        let frame_buffer_arc = Arc::new(Mutex::new([false; 64*32]));
        let latest_key = Arc::new(Mutex::new(None));
        let mut chip8: Chip8 = Default::default();
        chip8.run_instr(Opcode::Jmp(0x18E), &frame_buffer_arc, &latest_key);
        assert_eq!(chip8.pc, 0x18E);
    }

    #[test]
    fn test_run_instr_ldv_ldf_drw() {
        let frame_buffer_arc = Arc::new(Mutex::new([false; 64*32]));
        let latest_key = Arc::new(Mutex::new(None));
        let mut chip8: Chip8 = Default::default();
        chip8.run_instr(Opcode::LdV(0x00, 0x05), &frame_buffer_arc, &latest_key);
        assert_eq!(chip8.pc, 0x0202);
        assert_eq!(chip8.gen_regs[0], 0x0005);
        chip8.run_instr(Opcode::LdF(0x00), &frame_buffer_arc, &latest_key);
        assert_eq!(chip8.pc, 0x0204);
        assert_eq!(chip8.i_reg, 0x0019);
        chip8.run_instr(Opcode::Drw(0x1, 0x2, 0x3), &frame_buffer_arc, &latest_key);
    }
}