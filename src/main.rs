pub mod parser;

mod chip8;

use std::env;
use sdl2::pixels::Color;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use std::time::Duration;
use std::thread;
use std::sync::{Arc, Mutex};
use std::fs;


const SCALE: u32 = 20;

fn main() -> Result<(), String> {
    let args: Vec<String> = env::args().collect();

    let file_name = &args[1];
    let rom = fs::read(file_name).unwrap();
    let mut chip8: chip8::Chip8 = Default::default();

    chip8.load_rom(rom);
    let chip8: Arc<Mutex<chip8::Chip8>> = Arc::new(Mutex::new(chip8));
    let frame_buffer_arc = Arc::new(Mutex::new([false; 64*32]));

    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem.window("Chip-8 Emulator", 64*SCALE, 32*SCALE)
        .position_centered()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().build().unwrap();

    canvas.set_draw_color(Color::RGB(0, 0, 0));
    canvas.clear();
    canvas.present();

    let mut event_pump = sdl_context.event_pump().unwrap();

    let latest_key = Arc::new(Mutex::new(None));
    let set_key = Arc::clone(&latest_key);
    let chip8_instr = Arc::clone(&chip8);
    let frame_buffer = Arc::clone(&frame_buffer_arc);
    thread::spawn(move || {
        loop {
            {
                let mut chip8 = chip8_instr.lock().unwrap();
                let instr = chip8.get_instr();
                chip8.run_instr(instr, &frame_buffer, &latest_key);
            }

            ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 600));
        }
    });

    let mut looped = 0;
    'running: loop {
        canvas.set_draw_color(Color::RGB(0, 0, 0));
        canvas.clear();
        {
            let frame_buffer = frame_buffer_arc.lock().unwrap();
            canvas.set_draw_color(Color::RGB(255, 255, 255));
            for x in 0..64 {
                for y in 0..32 {
                    if frame_buffer[x+(y*64)] {
                        for x_scale in 0..SCALE {
                            for y_scale in 0..SCALE {
                                let p = sdl2::rect::Point::new((x*SCALE as usize + x_scale as usize) as i32, (y*SCALE as usize + y_scale as usize) as i32);
                                canvas.draw_point(p)?;
                            }
                        }
                    }
                }
            }
        }
        
        {
            let mut chip8 = chip8.lock().unwrap();
            chip8.apply_60hz();
        }

        for event in event_pump.poll_iter() {
            let mut set_key = set_key.lock().unwrap();
            match event {
                Event::Quit { .. } => {
                    break 'running
                },
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'running
                },
                Event::KeyDown { keycode: Some(Keycode::Num1), .. } => {
                    *set_key = Some(chip8::KeyCommand::Zero);
                },
                Event::KeyDown { keycode: Some(Keycode::Num2), .. } => {
                    *set_key = Some(chip8::KeyCommand::One);
                },
                Event::KeyDown { keycode: Some(Keycode::Num3), .. } => {
                    *set_key = Some(chip8::KeyCommand::Two);
                },
                Event::KeyDown { keycode: Some(Keycode::Num4), .. } => {
                    *set_key = Some(chip8::KeyCommand::Three);
                },
                Event::KeyDown { keycode: Some(Keycode::Q), .. } => {
                    *set_key = Some(chip8::KeyCommand::Four);
                },
                Event::KeyDown { keycode: Some(Keycode::W), .. } => {
                    *set_key = Some(chip8::KeyCommand::Five);
                },
                Event::KeyDown { keycode: Some(Keycode::E), .. } => {
                    *set_key = Some(chip8::KeyCommand::Six);
                },
                Event::KeyDown { keycode: Some(Keycode::R), .. } => {
                    *set_key = Some(chip8::KeyCommand::Seven);
                },
                Event::KeyDown { keycode: Some(Keycode::A), .. } => {
                    *set_key = Some(chip8::KeyCommand::Eight);
                },
                Event::KeyDown { keycode: Some(Keycode::S), .. } => {
                    *set_key = Some(chip8::KeyCommand::Nine);
                },
                Event::KeyDown { keycode: Some(Keycode::D), .. } => {
                    *set_key = Some(chip8::KeyCommand::A);
                },
                Event::KeyDown { keycode: Some(Keycode::F), .. } => {
                    *set_key = Some(chip8::KeyCommand::B);
                },
                Event::KeyDown { keycode: Some(Keycode::Z), .. } => {
                    *set_key = Some(chip8::KeyCommand::C);
                },
                Event::KeyDown { keycode: Some(Keycode::X), .. } => {
                    *set_key = Some(chip8::KeyCommand::D);
                },
                Event::KeyDown { keycode: Some(Keycode::C), .. } => {
                    *set_key = Some(chip8::KeyCommand::E);
                },
                Event::KeyDown { keycode: Some(Keycode::V), .. } => {
                    *set_key = Some(chip8::KeyCommand::F);
                },
                _ => {},
            }
        }

        canvas.present();
        ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));

        if looped == 10 { //Set key to none after 10/60 of a second
            let mut set_key = set_key.lock().unwrap();
            *set_key = None;
            looped = 0;
        }

        looped += 1;

    }

    Ok(())
}
